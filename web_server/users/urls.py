from django.conf.urls import url

from .views import Login
from .views import RegistrationView

urlpatterns = [
    url(r'^login/$', Login.as_view(), name='login'),
    url(r'^register/$', RegistrationView.as_view(), name='register'),
]
