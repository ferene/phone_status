from django.contrib.auth import logout, login
from django.contrib.auth.forms import AuthenticationForm
from django.core.urlresolvers import reverse, reverse_lazy
from django.http.response import HttpResponseRedirect
from django.contrib import messages

from django.utils.decorators import method_decorator
from django.views.decorators.debug import sensitive_post_parameters
from django.views.generic import CreateView
from django.views.generic.edit import FormView

from .forms import RegistrationForm
from .models import User


class Login(FormView):
    form_class = AuthenticationForm
    template_name = "login.html"

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(Login, self).get(request, *args, **kwargs)

    def form_valid(self, form):
        redirect_to = reverse('statuses')
        login(self.request, form.get_user())
        if self.request.session.test_cookie_worked():
            self.request.session.delete_test_cookie()
        return HttpResponseRedirect(redirect_to)

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))

    @method_decorator(sensitive_post_parameters('password'))
    def dispatch(self, request, *args, **kwargs):
        request.session.set_test_cookie()
        return super(Login, self).dispatch(request, *args, **kwargs)


class RegistrationView(CreateView):
    template_name = 'register.html'
    form_class = RegistrationForm
    model = User
    success_url = reverse_lazy('login')

    def form_valid(self, form):
        messages.success(self.request,
                         "Successfully registered. You can now login")
        return super(RegistrationView, self).form_valid(form)
