from django.conf.urls import include, url
from django.contrib import admin
from phone_status.views import StatusView
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^statuses/', include('phone_status.urls')),
    url(r'^users/', include('users.urls')),
]

urlpatterns += staticfiles_urlpatterns()
