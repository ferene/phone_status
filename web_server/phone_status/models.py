from django.db import models

import uuid
import base64


class HashMixin(models.Model):
    hash = models.CharField(
        max_length=9,
        verbose_name='hash',
        unique=True,
        null=True,
        editable=False
    )

    def save(self, *args, **kwargs):
        if not self.hash:
            self.hash = self.get_unique_hash()
        super(HashMixin, self).save(*args, **kwargs)

    class Meta:
        abstract = True

    @classmethod
    def calculate_hash(cls):
        return base64.urlsafe_b64encode(
            uuid.uuid4().bytes)[:cls._meta.get_field('hash').max_length]

    @classmethod
    def get_unique_hash(cls):
        hash = cls.calculate_hash()
        while cls.objects.all().filter(hash=hash).exists():
            hash = cls.calculate_hash()
        return hash


class Telephone(models.Model):
    hash = models.IntegerField()
    name = models.TextField(max_length=128)
    description = models.TextField(blank=True, null=True)
    user = models.ForeignKey('users.User')

    def __str__(self):
        message = '{0}({2}) of {1}' if self.description else '{0} of {1}'
        return message.format(self.name, self.user, self.description)


class PhoneStatus(models.Model):
    datetime = models.DateTimeField(
        auto_now_add=True,
    )
    application_name = models.TextField()
    start = models.DateTimeField()
    end = models.DateTimeField()
    telephone = models.ForeignKey(Telephone, on_delete=models.SET_NULL,
                                  null=True)
