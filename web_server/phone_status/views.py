from annoying.decorators import ajax_request
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.core.exceptions import ImproperlyConfigured
from django.http.response import HttpResponse
from django.shortcuts import get_object_or_404
from django.template.context import RequestContext
from django.template.loader import render_to_string
from django.template.response import TemplateResponse
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView
from django.views.generic.list import ListView

from .forms import TelephoneForm
from .models import Telephone
from .models import PhoneStatus

import mmh3


class StatusView(ListView):
    model = PhoneStatus
    template_name = 'statuses.html'

    def get_queryset(self):
        return super(StatusView, self).get_queryset().order_by('-datetime')


class UserStatusView(LoginRequiredMixin, StatusView):
    def get_queryset(self):
        telephones = Telephone.objects.filter(user=self.request.user)
        return super(UserStatusView, self).get_queryset().filter(
                telephone__in=telephones)

    def get_context_data(self, **kwargs):
        context = super(UserStatusView, self).get_context_data(**kwargs)
        context['custom'] = True
        return context


# ////////////////////////////////////////////////


class DecoratedDispatchMixin(object):
    def __init__(self, *args, **kwargs):
        self.dispatch_decorators = []
        super(DecoratedDispatchMixin, self).__init__(*args, **kwargs)

    def dispatch(self, *args, **kwargs):
        decorators = self.get_dispatch_decorators()
        dispatch = super(DecoratedDispatchMixin, self).dispatch
        for decorator in decorators:
            dispatch = decorator(dispatch)
        return dispatch(*args, **kwargs)

    def get_dispatch_decorators(self):
        return self.dispatch_decorators


def DispatchDecoratedWith(*new_decorators):
    class _DecoratedWith(DecoratedDispatchMixin):
        def get_dispatch_decorators(self):
            decorators = super(_DecoratedWith, self).get_dispatch_decorators()
            decorators.extend(new_decorators)
            return decorators

    return _DecoratedWith

JsonResponseMixin = DispatchDecoratedWith(ajax_request)

class HtmlInDictMixin(object):
    """View returning dict response with generated text as 'html' attribute.

    Superclass' `dispatch` should return html code, which would be
    put into dict response. If it returns None, html attribute
    in response is empty.

    Method `get_response_data` can be overriden to add more data to response.
    Another way to add custom data is to use response_data attribute.
    """
    def dispatch(self, request, *args, **kwargs):
        response = super(HtmlInDictMixin, self).dispatch(
            request, *args, **kwargs)
        if isinstance(response, HttpResponse) or isinstance(response, dict):
            return response
        return self._response_data(response_html=response)

    def _response_data(self, response_html):
        data = self.get_response_data()
        if response_html is not None:
            data['html'] = response_html
        return data

    def get_response_data(self, *args, **kwargs):
        return getattr(self, 'response_data', {})


class HtmlInJsonMixin(JsonResponseMixin, HtmlInDictMixin):
    pass


class TemplateResponseMixin(object):
    """
    A mixin that can be used to render a template.
    """
    template_name = None
    response_class = TemplateResponse
    content_type = None

    def render_to_response(self, context, **response_kwargs):
        """
        Returns a response, using the `response_class` for this
        view, with a template rendered with the given context.

        If any keyword arguments are provided, they will be
        passed to the constructor of the response class.
        """
        response_kwargs.setdefault('content_type', self.content_type)
        return self.response_class(
            request = self.request,
            template = self.get_template_names(),
            context = context,
            **response_kwargs
        )

    def get_template_names(self):
        """
        Returns a list of template names to be used for the request. Must return
        a list. May not be called if render_to_response is overridden.
        """
        if self.template_name is None:
            raise ImproperlyConfigured(
                "TemplateResponseMixin requires either a definition of "
                "'template_name' or an implementation of 'get_template_names()'")
        else:
            return [self.template_name]


class TemplateStringMixin(TemplateResponseMixin):
    """Mixin's  `render_to_response` method returns string instead of
    TemplateResponse.

    TemplateMixin's `render_to_response` method uses self.response_class to
    construct a response (TemplateResponse by default). Here we put
    method instead of class, constructing just string.

    Usage:
    class TestView(TemplateResponseMixin, View):
        template_name = 'some_template.html'

        def get(request):
            return self.render_to_response({'foo': 'bar'})
    Result: rendered HTML as a string.
    """

    def response_class(self, request, template, context, **kwargs):
        """Method pretending constructor of response, used by TemplateMixin."""
        return render_to_string(template, context, RequestContext(request))


class TemplateHtmlInJsonMixin(HtmlInJsonMixin, TemplateStringMixin):
    pass


# ////////////////////////////////////////////////


class TelephonesList(LoginRequiredMixin, ListView):
    model = Telephone
    template_name = 'telephones.html'

    def get_queryset(self):
        return super(TelephonesList, self).get_queryset().filter(
                user=self.request.user)


class TelephoneFormView(LoginRequiredMixin, TemplateHtmlInJsonMixin, FormView):
    form_class = TelephoneForm
    template_name = 'telephone_form.html'

    def form_valid(self, form):
        Telephone.objects.create(
            user=self.request.user,
            name=form.cleaned_data['name'],
            description=form.cleaned_data['description'],
            hash=mmh3.hash(form.cleaned_data['password']))
        return {'success': True}


class DeleteTelephoneView(LoginRequiredMixin, TemplateHtmlInJsonMixin,
                          TemplateView):
    template_name = 'telephone_delete.html'

    def get_context_data(self, **kwargs):
        context = super(DeleteTelephoneView, self).get_context_data(**kwargs)
        context['telephone'] = get_object_or_404(
            Telephone, id=kwargs['telephone_id'])
        return context

    def post(self, request, **kwargs):
        telephone = get_object_or_404(
            Telephone, id=kwargs['telephone_id'])
        assert self.request.user == telephone.user
        telephone.delete()
        messages.success(self.request,
                         'Telephone {} deleted'.format(telephone.name))
        return {'success': True}
