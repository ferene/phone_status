from django import forms
from django.forms.models import ModelForm
from django.forms.widgets import Textarea, PasswordInput

from .models import Telephone


class TelephoneForm(ModelForm):
    password = forms.CharField(max_length=40, widget=PasswordInput(
            attrs={'class': 'form-control'}))

    class Meta:
        model = Telephone
        fields = ['name', 'description']
        widgets = {
            'name': Textarea(
               attrs={'rows': 1, 'cols': 20, 'class': 'form-control'}
            ),
            'description': Textarea(
               attrs={'rows': 2, 'cols': 40, 'class': 'form-control'}
            ),
        }
