$( document ).ready(function() {
    $("#add_telephone_btn").click(function(){
        var modal = $("#action-modal");
        var url = $(this).data('url');
        new TelephoneForm(url);
    });
    $(".delete-telephone-button").click(function(){
        var modal = $("#action-modal");
        var url = $(this).data('url');
        new DeleteTelephoneController(url);
    })

});


function TelephoneForm(url) {
    var self = this;
    var modal = $('#action-modal');
    var modal_content = $('.modal-content', modal);

    self.display = function (html) {
        modal_content.html(html);
        modal.off('click', '#confirm-telephone-creation').one('click', '#confirm-telephone-creation', self.saveForm);
    };

    self.getForm = function () {
        modal.modal();
        $.get(url, function (data) {
            self.display(data.html);
        });
    };

    self.saveForm = function () {
        var request_data = modal.find('form').serialize();
        $.post(url, request_data,
               function (data) {
                   if (data.success == true) {
                       modal.modal('hide');
                       location.reload();
                   }
                   if (data.html) {
                       self.display(data.html);
                   }
               }
        );
    };
    self.getForm();
}

function DeleteTelephoneController(url) {
    var self = this;
    var modal = $('#action-modal');
    var modal_content = $('.modal-content', modal);

    self.display = function (html) {
        modal_content.html(html);
        modal.off('click', '#confirm-telephone-delete').one('click', '#confirm-telephone-delete', self.saveForm);
    };

    self.getForm = function () {
        modal.modal();
        $.get(url, function (data) {
            self.display(data.html);
        });
    };

    self.saveForm = function () {
        var request_data = modal.find('form').serialize();
        $.post(url, request_data, function (data) {
            if (data.success == true) {
                modal.modal('hide');
                location.reload();
            }
            if (data.html) {
                self.display(data.html);
            }
        }
        );
    };
    self.getForm();
}
