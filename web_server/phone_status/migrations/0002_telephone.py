# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('phone_status', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Telephone',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('hash', models.CharField(verbose_name=b'hash', max_length=9, unique=True, null=True, editable=False)),
                ('name', models.TextField(max_length=128)),
                ('description', models.TextField()),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
