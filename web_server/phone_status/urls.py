from django.conf.urls import url

from .views import DeleteTelephoneView
from .views import UserStatusView
from .views import TelephoneFormView
from .views import TelephonesList, StatusView

urlpatterns = [
    url(r'^telephones/$', TelephonesList.as_view(), name='telephones'),
    url(r'^telephone/create$', TelephoneFormView.as_view(),
        name='telephone_add'),
    url(r'^telephone/delete/(?P<telephone_id>\d+)/$',
        DeleteTelephoneView.as_view(), name='telephone_delete'),
    url(r'^all/$', StatusView.as_view(), name='statuses'),
    url(r'^my/$', UserStatusView.as_view(), name='my_statuses'),
]
