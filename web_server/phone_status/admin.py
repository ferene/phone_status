from django.contrib import admin

from .models import PhoneStatus


class StatusAdmin(admin.ModelAdmin):
    list_display = ['application_name', 'telephone', 'start', 'end']


admin.site.register(PhoneStatus, StatusAdmin)
