#include <iostream>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <pqxx/pqxx>

#define SERVER_PORT 7234
#define QUEUE_SIZE 5

using namespace std;
using namespace pqxx;


int insert(string message)
{
    string sql;
    try{
        connection C("dbname=tin user=postgres password=qwerty1asd hostaddr=127.0.0.1 port=5432");
        if (C.is_open()) {
            cout << "Opened database successfully: " << C.dbname() << endl;
        } else {
            cout << "Can't open database" << endl;
            return 1;
        }
        /* Create SQL statement */
        sql = "INSERT INTO phone_status_phonestatus (MESSAGE, DATETIME) VALUES ('" + message + "', CURRENT_TIMESTAMP);";

        /* Create a transactional object. */
        work W(C);

        /* Execute SQL query */
        W.exec( sql );
        W.commit();
        cout << "Records created successfully" << endl;
        C.disconnect ();
    }catch (const std::exception &e){
        cerr << e.what() << std::endl;
        return 1;
    }
}


int main()
{
    int nSocket, nClientSocket;
    int nFoo = 1;
    socklen_t nTmp;
    struct sockaddr_in stAddr, stClientAddr;

    /* address structure */
    memset(&stAddr, 0, sizeof(struct sockaddr));
    stAddr.sin_family = AF_INET;
    stAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    stAddr.sin_port = htons(SERVER_PORT);

    /* create a socket */
    nSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (nSocket < 0)
    {
        fprintf(stderr, "Can't create a socket.\n");
        exit(1);
    }
    setsockopt(nSocket, SOL_SOCKET, SO_REUSEADDR, (char*)&nFoo, sizeof(nFoo));
    /* bind a name to a socket */
    if (bind(nSocket, (struct sockaddr*)&stAddr, sizeof(struct sockaddr)) < 0)
    {
        fprintf(stderr, "Can't bind a name to a socket.\n");
        exit(1);
    }
    /* specify queue size */
    if (listen(nSocket, QUEUE_SIZE) < 0)
    {
        fprintf(stderr, "Can't set queue size.\n");
    }

    while(1)
    {
        /* block for connection request */
        nTmp = sizeof(struct sockaddr);
        nClientSocket = accept(nSocket, (struct sockaddr*)&stClientAddr, &nTmp);
        if (nClientSocket < 0)
        {
            fprintf(stderr, "Can't create a connection's socket.\n");
            exit(1);
        }
        int buffer_size = 1024;
        printf("[connection from %s]\n", inet_ntoa(stClientAddr.sin_addr));
        uint32_t  nlength, total;
        recv(nClientSocket ,(char*)&nlength, 4, 0);
        nlength = ntohl(nlength);
        cout<<"Total: "<<nlength<<endl;
        total = nlength;
        string result = "";
        if(total>buffer_size)
        {
            char * full_buf = new char[nlength];
            uint32_t curr = 0;
            uint32_t curr_len = buffer_size;
            while(curr < nlength)
            {
                curr_len = buffer_size;
                char * buf = new char[curr_len];
                memset(buf, 0, curr_len);
                cout <<"reading: "<<curr_len<<endl;
                recv(nClientSocket, buf, curr_len, 0);
                cout<<"Read ("<<curr_len<<"): "<<buf<<endl;
                strcat(full_buf, buf);
                curr+=curr_len;
                usleep(100000);
            }
            cout <<"message: " <<full_buf<<endl;
            result = string(full_buf);
        }
        else
        {
        printf("Alloc: %d\n", nlength);
        char *recvbuf = new char[nlength];
        recv(nClientSocket, recvbuf, nlength, 0);
        printf("Received: %s \n", recvbuf);
        result = string(recvbuf);
        }
        insert(result);
        close(nClientSocket);
    }
}
