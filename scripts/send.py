import struct
import socket
import sys
from contextlib import contextmanager


@contextmanager
def socket_context(*args, **kw):
    s = socket.socket(*args, **kw)
    try:
        yield s
    finally:
        s.close()


def send(message='Hello, world', host='localhost', port=7234):
    with socket_context(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port))
        print len(message), message
        s.sendall(struct.pack('>I', len(message)) + message)

if __name__ == '__main__':
    if len(sys.argv) > 1:
        message = ' '.join(sys.argv[1:])
        send(message)
    else:
        send()
