cmake_minimum_required(VERSION 3.1)
project(phone_status_BSD)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -lpqxx -lpq")

set(SOURCE_FILES server_BSD/main.cpp)
add_executable(phone_status_BSD ${SOURCE_FILES})

target_link_libraries(phone_status_BSD pqxx pq)
